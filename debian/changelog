vis (0.9-1) unstable; urgency=medium

  * New upstream version 0.8 (Closes: #1050536)
  * d/control: bump Standards-Version to 4.7.0, no changes needed
  * d/control: replace obsolete B-D on libncursesw5-dev with libncurses-dev
  * d/control: replace obsolete B-D on pkg-config with pkgconf
  * d/control: build against newer liblua (liblua5.4)
  * d/rules: drop override_dh_auto_test, made unnecessary by upstream change
  * d/rules: turn override_dh_auto_clean into execute_after_dh_auto_clean
  * d/copyright: update copyright years for debian/*

 -- Paride Legovini <paride@debian.org>  Fri, 10 May 2024 15:58:40 +0200

vis (0.8-1) unstable; urgency=medium

  * New upstream version 0.8
  * d/control: bump Standards-Version to 4.6.1, no changes needed
  * d/copyright: update copyright years for debian/*
  * d/lintian-overrides: refresh overrides
  * d/watch: switch to mode=git.
    We only use the watch file to detect new upstream versions, as the
    packaging follows a pure-git workflow (gbp import-ref). What we care
    about are git tags, so let's directly watch for those.

 -- Paride Legovini <paride@debian.org>  Mon, 14 Nov 2022 09:47:41 +0000

vis (0.7-2) unstable; urgency=medium

  * d/control: drop the 9base B-Dep (RC-buggy test dependency).
  * d/*: apply wrap-and-sort -bast (cosmetic).

 -- Paride Legovini <paride@debian.org>  Wed, 03 Mar 2021 16:20:36 +0000

vis (0.7-1) unstable; urgency=medium

  * New upstream version 0.7.
  * d/control: Bump Standards-Version to 4.5.1, no changes needed.
  * d/gbp.conf: debian-branch = debian/latest (DEP-14).
  * d/s/lintian-overrides: override package-does-not-install-examples for
    test/sam/examples/. We're not supposed to install the test suite examples.

 -- Paride Legovini <paride@debian.org>  Wed, 06 Jan 2021 23:02:44 +0000

vis (0.6-3) unstable; urgency=medium

  * Run the tests only on: amd64, arm64, ppc64el.

 -- Paride Legovini <paride@debian.org>  Wed, 29 Jul 2020 22:59:29 +0000

vis (0.6-2) unstable; urgency=medium

  * d/control: Build-Depend on vim and 9base only on amd64 due to
    package availability and flaky tests.
  * d/gbp.conf: move the pristine-tar settings to the DEFAULT section.

 -- Paride Legovini <paride@debian.org>  Tue, 07 Jul 2020 18:17:56 +0000

vis (0.6-1) unstable; urgency=medium

  * New upstream version 0.6.
  * d/control:
    - Bump Standards-Version to 4.5.0 (no changes needed).
    - Bump the dh compat level to 13.
    - Build-Depend on 9base and vim (needed test against sam and vim).
    - Use the paride@debian.org alias in Uploaders.
    - Add Multi-Arch: foreign.
    - Add Rules-Requires-Root: no.
    - Rewrap the file (wrap-and-sort -st).
  * d/gbp.conf:
    - Unset upstream-branch (imports are done from upstream git).
    - Always create the (xz compressed) pristine-tar.
  * d/rules:
    - Re-enable the vim tests.
    - Stop enforcing -Wl,--as-needed (now done by default).
    - Drop the test suite cleanup commands; use d/clean instead.
    - dh_auto_test: set LANG=C.UTF-8.
      See: https://github.com/martanne/vis-test/issues/25.
  * d/copyright: update copyright entry for debian/*.
  * autopkgtests:
    - Drop the needs-recommends requirement (deprecated).
    - Do not set LC_ALL=en_US.UTF-8 (now done upstream).
  * Drop all the patches (upstreamed).

 -- Paride Legovini <paride@debian.org>  Mon, 06 Jul 2020 22:33:26 +0000

vis (0.5+ts-3) unstable; urgency=medium

  * Disable the vim tests
  * Run the test/vis tests as autopkgtest

 -- Paride Legovini <pl@ninthfloor.org>  Tue, 08 May 2018 14:34:19 +0000

vis (0.5+ts-2) unstable; urgency=medium

  * Build-depend on libselinux1-dev only on Linux systems

 -- Paride Legovini <pl@ninthfloor.org>  Mon, 16 Apr 2018 21:46:38 +0000

vis (0.5+ts-1) unstable; urgency=medium

  * Enable the test suite (git submodule):
    - d/rules: do not override_dh_auto_test.
    - d/rules: cleanup the test suite after building.
    - d/gbp.conf: add ‘submodules = True’.
    - New build-deps: locales-all, lua-busted, lua-lpeg, vim.
    - Cover the test suite in d/copyright.
    - Add the ‘+ts’ version string to differentiate the new .orig
      tarball now containing the test suite from the previous one.
    - Patch: delete broken test depending on external sed(1)
      invocation (cherry-pick, upstream commit 90dd04f).
    - Patch: make sure $TERM is set (cherry pick, c824ec2).
  * Build with POSIX acl support (build-depend on libacl1-dev).
  * Build with SELinux support (build-depend on libselinux1-dev).
  * Bump Standards-Version to 4.1.4 (no changes needed).
  * debian/* relicensed as ISC.
  * d/watch: dversionmangle: remove version string

 -- Paride Legovini <pl@ninthfloor.org>  Mon, 16 Apr 2018 08:41:36 +0000

vis (0.5-1) unstable; urgency=medium

  * New upstream version 0.5
  * Drop upstreamed patches
  * New patches:
    - Remove execution file permission from clojure.lua
      (upstreamed, cherry-picked)
    - Remove broken manpage POSIX referemce
      (upstreamed, cherry-picked)
  * Add upstream metadata file (DEP12)
  * debian/copyright:
    - updated copyright years
    - added the new lexers
    - lexers copyright entries consolidated in a single section
    - point to ‘common-licenses’ instead of reporting the full CC0 license

 -- Paride Legovini <pl@ninthfloor.org>  Wed, 28 Mar 2018 07:19:54 +0000

vis (0.4-2) unstable; urgency=medium

  * Bump to Standards-Version 4.1.3
  * Bump compat level to 11
  * Packaging repository moved to salsa
  * Only link with needed libraries (ld --as-needed)

 -- Paride Legovini <pl@ninthfloor.org>  Mon, 19 Feb 2018 18:13:23 +0100

vis (0.4-1) unstable; urgency=medium

  * New upstream version
  * Bump debian/compat level to 10
  * Remove upstreamed patches
  * Patch the missing copyright notice for the strace lexer
  * Add debian/copyright entry for the strace lexer
  * Bump standards version
  * Remove extra license files
  * Add more options to d/gbp.conf
  * Add Vcs- fields to d/control
  * Specify the packaging branch in the Vcs-Git field
  * Fix d/gbp.conf (options must be specified without the 'git-' prefix)

 -- Paride Legovini <pl@ninthfloor.org>  Tue, 25 Jul 2017 15:22:33 +0200

vis (0.3-2) unstable; urgency=medium

  * Fix compilation on GNU Hurd systems (Closes: #860513)

 -- Paride Legovini <pl@ninthfloor.org>  Sat, 13 May 2017 12:29:13 +0200

vis (0.3-1) unstable; urgency=medium

  * Initial release. (Closes: #838895)

 -- Paride Legovini <pl@ninthfloor.org>  Fri, 31 Mar 2017 16:00:13 +0200
